import pickle


class Url(object):


    @classmethod
    def shorten(cls, full_url):
        """
        Acorta los Urls
        """

        # Crea una instancia de la clase Url
        instance           = cls()
        instance.full_url  = full_url
        instance.short_url = instance.__create_short_url()
        Url.__save_url_mapping(instance)

        return instance


    @classmethod
    def get_by_short_url(cls, short_url):
        """
        Regresa una instancia de Url correspondiente a short_url
        """

        url_mapping = Url.load_url_mapping()

        return url_mapping.get(short_url)


    def __create_short_url(self):
        """
        Crea un url corto, lo salva y lo regresa
        """

        last_short_url = Url.__load_last_short_url()
        short_url      = self.__increment_string(last_short_url)
        Url.__save_last_short_url(short_url)

        return short_url


    def __increment_string(self, string):
        """
        Incrementa un string:

            a  -> b
            z  -> aa
            az -> ba
        """

        if string == '':

            return 'a'

        last_char = string[-1]

        if last_char != 'z':

            return string[:-1] + chr(ord(last_char) + 1)

        return self.__increment_string(string[:-1]) + a


    @staticmethod
    def __load_last_short_url():
        """
        Regresa el ultimo url corto generado
        """

        try:

            return pickle.load(open('last_short.p', 'rb'))

        except IOError:

            return ''


    @staticmethod
    def __save_last_short_url(url):
        """
        Guarda el ultimo url corto generado
        """
        pickle.dump(url, open('last_short.p', 'wb'))


    @staticmethod
    def __load_url_mapping():
        """
        Regresa un diccionario de urls cortas mapeado a las
        urls
        """

        try:

            return pickle.load(open('short_to_url.p', 'rb'))

        except IOError:

            return {}


    @staticmethod
    def __save_url_mapping(instance):
        """
        Guarda la instancia papeada del url corto al url
        """

        short_to_url                     = Url.__load_url_mapping()
        short_to_url[instance.short_url] = instance
        pickle.dump(short_to_url, open('short_to_url.p', 'wb'))
